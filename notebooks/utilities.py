from typing import Callable
from ctypes import ArgumentError
import numpy as np
import matplotlib.pyplot as plt


class HertzContactJohnson:
    """Hertz contact solution for given force, radius and effective modulus.

    Args:
        - radius (float): Radius
        - E (float): Young's modulus
        - force (float): Total contact force
    """
    def __init__(self, radius, E, force) -> None:
        self.radius = radius
        self.E = E
        # self.Estar = self.E/(1-0.2**2)  # TODO: which to use (plain stress?)
        self.Estar = E
        self.force = force
        self.area = (4*self.force*self.radius / np.pi / self.Estar)**.5
        self.p0 = 2*self.force/np.pi/self.area

    def contact_pressure(self, x, normalize=True):
        p = self.p0*(1-(x**2/self.area**2))**.5
        p[np.isnan(p)] = 0
        if normalize:
            return p/self.p0
        else:
            return p


def integrate_axis(field: Callable, xi: np.array,
                          yi: np.array, axis: str = 'y') -> np.array:
    """Integrate in one axis for an array of positions.

    Args:
        - field: function to evaluate (must be able to process array)
        - xi: evaluation points x
        - yi: evaluation points y
        - axis: axis to integrate ('y', 'x')
    """
    if axis=='x':
        data = [np.trapz(field(xi, y), xi, axis=0) for y in yi]
    elif axis=='y':
        data = [np.trapz(field(x, yi), yi, axis=0) for x in xi]
    else:
        raise ArgumentError("Axis argument must be 'x' or 'y'")
    return np.array(data)


def subplot_label(ax, label, w_inches=-0.3, h_inches=.08):
    w, h = ax.get_figure().get_size_inches()
    ax.text(w_inches/ax.get_position().width/w, 1+h_inches/ax.get_position().height/h, rf'\textbf{{{label})}}', transform=ax.transAxes,
            va='bottom', fontfamily='serif', fontweight='bold',
            fontsize='large', )


def set_plot_theme():
    """Set plot theme."""

    fontsize = 8
    plt.rcParams.update({
        'axes.linewidth': .5, 
        'axes.grid': False,
        'axes.titlesize': fontsize, 
        'axes.labelsize': fontsize, 
        'grid.linewidth': .5, 
        'grid.linestyle': (5,8), 
        'lines.linewidth': .5, 
        'lines.markersize': 3, 
        'lines.markeredgewidth': .5, 
        'xtick.major.width': .5, 'xtick.minor.width': .3, 'xtick.minor.size': 2,
        'ytick.major.width': .5, 'ytick.minor.width': .3, 'ytick.minor.size': 2,
        'xtick.labelsize': fontsize, 
        'ytick.labelsize': fontsize, 
        'xtick.major.size': 3, 
        'ytick.major.size': 3,
        'xtick.direction': 'in',
        'ytick.direction': 'in',
        'figure.dpi': 150,
        'text.usetex': True,
        'font.size': fontsize, 
        'font.family': 'serif',
        'patch.linewidth': .3,
        'legend.fontsize': fontsize,
        'legend.fancybox': False,
        'legend.borderpad': 0.2,
        'legend.edgecolor': '0.5',
        'legend.framealpha': .8,
        'legend.handlelength': 1,
        'legend.shadow': False,
        'legend.labelspacing': .3,
    })
