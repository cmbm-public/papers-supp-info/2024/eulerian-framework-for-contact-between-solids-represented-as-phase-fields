# This file is part of the supplementary material for the publication:
# Eulerian framework for contact between solids represented as phase fields
# 
# Script for contact between a rigid plane and a deformable half-circle
# Uses:
#   - FEniCS
#   - bamboost
#   - phasontact
#
# Copyright Flavio Lorez 2023

import argparse
import logging
from mpi4py import MPI
import fenics as fe

log = logging.getLogger(__name__)

import phasontact
from phasontact import geometry, material, Body
import phasontact.model.equations as eqs
from bamboost import SimulationWriter


def main(path, uid):

    with SimulationWriter(uid, path) as sim:

        sim.add_metadata()
        params = sim.parameters
        phasontact.common.OPTS['penalty_constant'] = params['k']

        mesh = phasontact.Mesh().read(params['mesh'])
        sim.add_mesh(mesh.msh.coordinates(), mesh.msh.cells())
        log.info("mesh read")

        fe.set_log_level(30)

        model = phasontact.Model(params, mesh)

        # create bodies
        disk = model.add_body(
                geometry.Circular(params['r0'], params['eps'], (0., 0.)),
                material.LinearElastic(params['E'], params['poissons']),
                )
        wall = model.add_body(
                geometry.VerticalWallsSymmetric(params['r0'], params['eps'], axis='y'),
                material.LinearElastic(params['E'], params['poissons']),
                )
        model.generate_function_space()

        # Define boundary subdomains for BCs
        bottom = fe.CompiledSubDomain('on_boundary && near(x[1], 0.)')
        boundary = fe.CompiledSubDomain('on_boundary && !near(x[1], 0.)')
        top = fe.CompiledSubDomain('on_boundary && near(x[1], 20.0)')
        model.add_dirichlet_bc(disk.displacement_field, fe.Constant(0.), 
                               bottom, 'symmetry', sub_field='y')
        model.add_dirichlet_bc(disk.displacement_field, fe.Constant((0., 0.)),
                               boundary, 'fix')
        
        solver = phasontact.Solver(model)

        def mu_mid_point(body: Body) -> fe.Form:
            phi, phi0, _ = body.phasefield.get_components()
            mu, _, mu_test = body.chemical_energy_mu.get_components()
            eps = body.eps
            form = (
                    mu * mu_test * fe.dx
                    - eps**2 * fe.dot(fe.grad((phi+phi0)/2), fe.grad(mu_test)) * fe.dx
                    - (body.phasefield.G_(phi) + body.phasefield.G_(phi0))/2 * mu_test * fe.dx
                    )
            return form

        solver.form = (
                eqs.static_equilibrium(disk)
                + eqs.reference_map_advection(disk)
                + eqs.advective_cahn_hilliard(disk, include_chemical=False)
                + mu_mid_point(disk)
                - eqs.contact_force(disk, wall)
                + eqs.static_equilibrium(wall)
                + eqs.reference_map_advection(wall)
                + eqs.advective_cahn_hilliard(wall, include_chemical=False)
                + mu_mid_point(wall)
                - eqs.contact_force(wall, disk)
                )
        solver.initialize()

        solver_options = {"nonlinear_solver": "snes",
                          "snes_solver" : {
                              "linear_solver": 'mumps',
                              "preconditioner": 'amg',
                              "maximum_iterations": 40,
                              "relative_tolerance": 1e-9,
                              "absolute_tolerance": 1e-9,
                              "error_on_nonconvergence": True},
                          }
        solver.update_options(solver_options)

        log.info("model initiated")

        # Move the wall given the steps specified
        for i, r in enumerate(params['rigid_positions']):

            if i==0:
                u_move_wall = fe.Constant((0., 0.))
            else:
                u_move_wall = fe.Constant((0., r-params['rigid_positions'][i-1]))

            # move wall
            phi_new = wall.phi_prev - fe.dot(u_move_wall, fe.grad(wall.phi_prev))
            wall.phasefield.assign_prev(fe.project(phi_new, wall.phasefield.function_space()))
            refmap_new = fe.project(
                    wall.referencemap.function_prev - fe.dot(fe.grad(wall.referencemap.function_prev), u_move_wall),
                    wall.referencemap.function_space()
                    )
            wall.referencemap.assign_prev(refmap_new)
            wall.phasefield.assign(wall.phasefield.function_prev)
            wall.referencemap.assign(wall.referencemap.function_prev)

            # Add BCs for the wall
            model.add_dirichlet_bc(wall.displacement_field, fe.Constant(0.),
                                   top, 'symmetry-wall', sub_field='y')
            model.add_dirichlet_bc(wall.displacement_field, fe.Constant((0., 0.)),
                                   bottom, 'fix-wall')

            # Set displacement field to (0, 0) -> Initial guess
            disk.displacement_field.assign(fe.interpolate(fe.Constant((0., 0.)), disk.displacement_field.function_space()))
            wall.displacement_field.assign(fe.interpolate(fe.Constant((0., 0.)), wall.displacement_field.function_space()))

            solver.initialize()
            solver.update_options(solver_options)
            solver.solve()

            # Store the results of the step
            sim.add_field('disk_u', disk.displacement_field.get_nodal_values())
            sim.add_field('disk_phi', disk.phasefield.get_nodal_values())
            sim.add_field('disk_vonMises', model.get_nodal_values(disk.vonMises_stress()))
            sim.add_field('disk_sig', model.get_nodal_values(disk.stress()))
            sim.add_field('disk_b', model.get_nodal_values(eqs.contact_force_field(disk, wall)))
            sim.add_field('disk_X', disk.referencemap.get_nodal_values())

            sim.add_field('wall_u', wall.displacement_field.get_nodal_values())
            sim.add_field('wall_phi', wall.phasefield.get_nodal_values())
            sim.add_field('wall_vonMises', model.get_nodal_values(wall.vonMises_stress()))
            sim.add_field('wall_sig', model.get_nodal_values(wall.stress()))
            sim.add_field('wall_b', model.get_nodal_values(eqs.contact_force_field(wall, disk)))
            sim.add_field('wall_X', wall.referencemap.get_nodal_values())

            sim.add_global_field('wall_position', r)
            sim.add_global_field('traction_x', fe.assemble(eqs.contact_force_field(disk, wall)[0] * fe.dx))
            sim.add_global_field('traction_y', fe.assemble(eqs.contact_force_field(disk, wall)[1] * fe.dx))

            # Finish the step by updating the previous referencemap and phasefield
            disk.update_fields()
            wall.update_fields()

            sim.finish_step()

            if MPI.COMM_WORLD.rank==0:
                print(f'Finished step {i}, wall position = {r}')

        sim.finish_sim()
        sim.create_xdmf_file()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str)
    parser.add_argument('--uid', type=str)
    args = parser.parse_args()

    main(args.path, args.uid)




