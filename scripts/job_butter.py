# This file is part of the supplementary material for the publication:
# Eulerian framework for contact between solids represented as phase fields
# 
# Script for contact between a rigid plane and a deformable half-circle
# Uses:
#   - bamboost
#
# Copyright Flavio Lorez 2023

from bamboost import Manager
import os

# --------------------------------------------------------------------------

# Specify database path
database_path = '../out-butterfly/'
db = Manager(database_path)

name = 'butterfly_july'
script = os.path.abspath('./script_butterfly.py')

# Define parameter dictionary
parameters = {
        'problem_type': 'deformable-deformable',
        'eps': 0.2,
        'mobility': 1e-4,
        'E': 1.,
        'poissons': 0.2,
        'k': 1e1,
        'mesh': os.path.abspath('../mesh/100x150_fine.xdmf'),
        'residual_stiffness': 1e-2,
        'disk_pos': [6.237, 9.402],
        'butterfly': 'figs/butterfly2.png',
        'r0': 2.0,
        'nb_steps': 50,
        'grow_step': 1e-2,
}

# --------------------------------------------------------------------------

sim = db.create_simulation(name)
sim.add_parameters(parameters)
sim.copy_executable(script)
sim.copy_file('./job_butter.py')
sim.create_batch_script(nnodes=1, ntasks=6,
                        ncpus=1, time='04:00:00', mem_per_cpu=2048, euler=False)
print(sim.uid)

# --------------------------------------------------------------------------
