# This file is part of the supplementary material for the publication:
# Eulerian framework for contact between solids represented as phase fields
# 
# Script for contact between a rigid plane and a deformable half-circle
# Uses:
#   - FEniCS
#   - bamboost
#   - phasontact
#
# Copyright Flavio Lorez 2023

import argparse
from mpi4py import MPI
import fenics as fe

from bamboost import SimulationWriter
import phasontact
import phasontact.model.equations as eqs

# setting some global fenics options
fe.set_log_level(30)
# ---------------------------------------------------------
# ---------------------------------------------------------


def main(path: str, uid: str):

    with SimulationWriter(uid, path) as sim:

        sim.register_git_attributes('/cluster/work/cmbm/florez/growth-phasefield/')
        sim.register_git_attributes('/cluster/home/florez/dev/phasontact/')
        sim.register_git_attributes('/cluster/home/florez/dev/dbmanager/')

        sim.add_metadata()
        parameters = sim.parameters  # loading parameters
        box_domain = parameters['box_domain']
        mesh_top_edge = parameters['mesh_top_edge']

        # ===============================================================================
        # MODEL SETUP
        # ===============================================================================

        phasontact.common.OPTS['penalty_constant'] = parameters['k']
        sim.update_parameters({'material': 'Neo-hookean'})

        # load and write mesh
        mesh = phasontact.mesh.Mesh().read(parameters['mesh'])
        sim.add_mesh(mesh.msh.coordinates(), mesh.msh.cells())

        # phasontact model setup
        model = phasontact.Model(parameters, mesh)

        disk = model.add_body(
                phasontact.geometry.Circular(r0=parameters.get('radius'),
                                             eps=parameters.get('eps'),
                                             center=parameters.get('disk_positions')[0]),
                phasontact.material.NeoHookeanElastic(parameters.get('E'),
                                                      parameters.get('poissons')),
                )

        box = model.add_body(
                phasontact.geometry.Box(*box_domain, parameters.get('eps')),
                phasontact.material.NeoHookeanElastic(parameters.get('E'),
                                                    parameters.get('poissons')),
                )

        model.generate_function_space()

        # Boundary conditions
        bottom = fe.CompiledSubDomain('on_boundary && near(x[1], 0.)')
        top = fe.CompiledSubDomain(f'on_boundary && near(x[1], {mesh_top_edge})')
        left = fe.CompiledSubDomain(f'on_boundary && near(x[0], 0.)')

        # Fix box at the bottom
        model.add_dirichlet_bc(box.displacement_field, fe.Constant((0., 0.)), bottom)

        # Get the solver and define the variational form
        solver = phasontact.Solver(model)
        solver.form = model.formcollection.multi_body_crank_nicolson()
        solver.initialize()
        
        solver_options = {"nonlinear_solver": "snes",  # constrain
                          "snes_solver" : {
                              "linear_solver": 'mumps',
                              "preconditioner": 'amg',
                              "maximum_iterations": 20,
                              "relative_tolerance": 1e-9,
                              "absolute_tolerance": 1e-9,
                              "error_on_nonconvergence": False},
                          }
        solver.update_options(solver_options)
      
        disk_positions = parameters.get('disk_positions')


        # ===============================================================================
        # START SOLVING
        # ===============================================================================

        for i, position_step in enumerate(disk_positions):

            timestep_reduced = 0
        
            if i==0:
                u_disk = [0., 0.]
                position = position_step
            else:
                u_disk = position_step - disk_positions[i-1]
                position = disk_positions[i-1]

            while True:  # Loop which reduces the step size if convergence not possible

                position += u_disk
                if MPI.COMM_WORLD.rank==0: print(position)

                # set initial guesses
                disk.displacement_field.assign(fe.interpolate(fe.Constant(u_disk), disk.displacement_field.function_space()))
                box.displacement_field.assign(fe.interpolate(fe.Constant((0., 0.)), box.displacement_field.function_space()))

                # set phi to previous phi
                disk.phasefield.assign(disk.phasefield.function_prev)
                box.phasefield.assign(box.phasefield.function_prev)

                # --------------------------------------------------------------------------------
                # Strain sub problem
                # --------------------------------------------------------------------------------

                # find mid-node of disk to apply BCs
                closest_vertex_global = model.get_closest_vertex(position)
                mid_plane = fe.CompiledSubDomain(f'near(x[1], {closest_vertex_global[1]})')
                mid_plane_x = fe.CompiledSubDomain(f'near(x[1], {closest_vertex_global[1]}) && near(x[0], {closest_vertex_global[0]}, 0.2)')

                model.add_dirichlet_bc(disk.displacement_field, fe.Constant(u_disk[0]),
                                       mid_plane_x, 'move_x', 'x', 'pointwise')
                model.add_dirichlet_bc(disk.displacement_field, fe.Constant(u_disk[1]),
                                       mid_plane, 'move_y', 'y', 'pointwise')
                model.add_dirichlet_bc(disk.phasefield, fe.Constant(0.), top, name='disk_phi_fix')
                model.add_dirichlet_bc(box.phasefield, fe.Constant(0.), left, name='box_phi_fix')

                solver.initialize()
                solver.update_options(solver_options)
                status = solver.solve()

                # Check convergence
                if not status[1] and timestep_reduced<=2:
                    print('Not converged. Reduce step size')
                    position -= u_disk
                    u_disk = [.5*i for i in u_disk]
                    timestep_reduced += 1
                    continue
                if timestep_reduced>2:
                    print('Already reduced dt by factor 4, stopping simulation')
                    sim.finish_sim(status='Failed')
                    sim.create_xdmf_file()
                    return


                # --------------------------------------------------------------------------------
                # Output fields
                # --------------------------------------------------------------------------------

                sim.add_field('disk_phi', model.get_nodal_values(disk.phi))
                sim.add_field('disk_vonMises', model.get_nodal_values(disk.vonMises_stress()))
                sim.add_field('disk_sig', model.get_nodal_values(disk.stress()))
                sim.add_field('disk_X', disk.referencemap.get_nodal_values())
                sim.add_field('disk_u', disk.displacement_field.get_nodal_values())
                sim.add_field('box_phi', model.get_nodal_values(box.phi))
                sim.add_field('box_vonMises', model.get_nodal_values(box.vonMises_stress()))
                sim.add_field('box_sig', model.get_nodal_values(box.stress()))
                sim.add_field('box_X', box.referencemap.get_nodal_values())
                sim.add_field('box_u', box.displacement_field.get_nodal_values())
                sim.add_field('b', model.get_nodal_values(eqs.contact_force_field(disk, box)))

                # Compute reaction force
                mf = fe.MeshFunction('size_t', mesh.msh, 1, 0)
                top.mark(mf, 1)
                bottom.mark(mf, 2)
                left.mark(mf, 3)
                ds = fe.ds(subdomain_data=mf)

                sim.add_global_field('position_x', position[0])
                sim.add_global_field('position_y', position[1])
                sim.add_global_field('rf_vertical', fe.assemble(box.stress()[1, 1] * ds(2)))
                sim.add_global_field('rf_horizontal', fe.assemble(box.stress()[0, 0] * ds(2)))
                sim.add_global_field('rf_shear', fe.assemble(box.stress()[0, 1] * ds(2)))
                sim.add_global_field('contact_force_x', fe.assemble(eqs.contact_force_field(disk, box)[0] * fe.dx))
                sim.add_global_field('contact_force_y', fe.assemble(eqs.contact_force_field(disk, box)[1] * fe.dx))
                sim.add_global_field('disk_mass', fe.assemble(disk.density * fe.dx))
                sim.add_global_field('box_mass', fe.assemble(box.density * fe.dx))

                # update field variables
                disk.update_fields()
                box.update_fields()

                sim.finish_step()
                if MPI.COMM_WORLD.rank==0:
                    print(f'Solved step: {i}')

                # If reached next position, break while loop and continue
                if (abs(position[0]-position_step[0])<1e-6) and (abs(position[1]-position_step[1])<1e-6):
                    break

        sim.finish_sim()
        sim.create_xdmf_file()
    return 


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str)
    parser.add_argument('--uid', type=str)
    args = parser.parse_args()

    main(args.path, args.uid)
