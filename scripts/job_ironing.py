# This file is part of the supplementary material for the publication:
# Eulerian framework for contact between solids represented as phase fields
# 
# Script for contact between a rigid plane and a deformable half-circle
# Uses:
#   - bamboost
#
# Copyright Flavio Lorez 2023

from bamboost import Manager
import os
import numpy as np

# --------------------------------------------------------------------------

# Specify database path
database_path = '../out-ironing/'
db = Manager(database_path)

name = None
script = os.path.abspath('./script_ironing.py')

# Define parameter dictionary
parameters = {
        'problem_type': 'deformable-deformable',
        'eps': 0.1,
        'mobility': 1e-6,
        'radius': 1.,
        'E': 1.,
        'poissons': 0.2,
        'k': 2e2,
        'mesh': os.path.abspath('../mesh/60x45_120x90.xdmf'),
        'residual_stiffness': 1e-3,
        'box_domain': ([1, 3], [-4, 2]),
        'mesh_top_edge': 4.5,
}

# Define prescribed motion
nb_steps_down = 8
step_size = 0.05
y_start = 3.0
y_end = 2.5
x_start = 2.
x_end = 4.2 
disk_positions_1 = np.vstack((
    np.full(nb_steps_down, x_start),
    np.linspace(y_start, y_end, nb_steps_down, endpoint=False)
    )).T
x_positions = np.arange(x_start, x_end, step_size)
disk_positions_2 = np.vstack((
    x_positions, 
    np.full(x_positions.size, y_end),
    )).T
disk_positions = np.vstack((disk_positions_1, disk_positions_2))

parameters.update({'disk_positions': disk_positions})

# --------------------------------------------------------------------------

# Create new entry in database
sim = db.create_simulation(name)
sim.add_parameters(parameters)
sim.copy_executable(script)
sim.copy_file('./job_ironing.py')
sim.create_batch_script(nnodes=1, ntasks=6,
                        ncpus=1, time='04:00:00', mem_per_cpu=2048, euler=False)
print(sim.uid)

# --------------------------------------------------------------------------
