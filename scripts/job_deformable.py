# This file is part of the supplementary material for the publication:
# Eulerian framework for contact between solids represented as phase fields
# 
# Script for contact between a rigid plane and a deformable half-circle
# Uses:
#   - bamboost
#
# Copyright Flavio Lorez 2023

import os
import numpy as np

from bamboost import Manager

database_path = '../out'
db = Manager(database_path)

name = None
script = os.path.abspath('./script_deformable.py')

# Define parameter dictionary
parameters = {
        'problem_type': 'cranck nicolson implicit',
        'tag': 'def-def',
        'eps': .5,
        'mobility': 1e-6,
        'r0': 10.,
        'E': 1.,
        'poissons': 0.2,
        'k': 1e3,
        'mesh': os.path.abspath('../mesh/mesh_hertz_h20.xdmf'),
        'residual_stiffness': 1e-3,
        }

rigid_motion = np.linspace(parameters['r0'], parameters['r0']-0.1, 6)

parameters.update({'rigid_positions': rigid_motion})

# Create new entry in database
sim = db.create_simulation(name)
sim.add_parameters(parameters)
sim.copy_executable(script)
sim.copy_file('./job_deformable.py')
sim.create_batch_script(nnodes=1, ntasks=4,
                        ncpus=1, time='02:00:00', mem_per_cpu=2048, euler=False)

print(sim.uid)

