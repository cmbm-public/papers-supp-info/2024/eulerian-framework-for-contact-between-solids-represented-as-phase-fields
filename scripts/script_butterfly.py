# This file is part of the supplementary material for the publication:
# Eulerian framework for contact between solids represented as phase fields
# 
# Script for contact between growing disk and butterfly pore
# Uses:
#   - FEniCS
#   - bamboost
#   - phasontact
#
# Copyright Flavio Lorez 2023

import fenics as fe
from mpi4py import MPI
import argparse

from bamboost import SimulationWriter
from phasontact import Mesh, Model, geometry, material, Solver
import phasontact.model.equations as eqs


def main(path, uid):

    # Access simulation data previously created
    sim = SimulationWriter(uid, path)
    sim.add_metadata()
    params = sim.parameters

    # Read and store mesh
    mesh = Mesh().read(params['mesh'])
    sim.add_mesh(mesh.msh.coordinates(), mesh.msh.cells())

    fe.set_log_level(30)

    # Create the phasontact model
    model = Model(params, mesh)

    # Add the two bodies to the system
    disk = model.add_body(
            geometry.Circular(params['r0'], params['eps'], params['disk_pos']),
            material.NeoHookeanElastic(params['E1'], params['poissons1']),
            name='disk',
            )
    butterfly = model.add_body(
            geometry.FromImage(params['butterfly'], [[0, 10], [0, 15]], params['eps']),
            material.NeoHookeanElastic(params['E2'], params['poissons2']),
            name='butterfly',
            )
    model.generate_function_space()

    # Define boundary domains and BCs
    right = fe.CompiledSubDomain('on_boundary && near(x[0], 10.)')
    left = fe.CompiledSubDomain('on_boundary && near(x[0], 0.)')

    model.add_dirichlet_bc(disk.displacement_field, fe.Constant((0., 0.)),
                           'near(x[0], 0.) && near(x[1], 0.)', 'fix_disk', None, 'pointwise')
    model.add_dirichlet_bc(butterfly.displacement_field, fe.Constant(0.),
                           right, name='butter_symmetry', sub_field='x')
    model.add_dirichlet_bc(butterfly.displacement_field, fe.Constant((0., 0.)),
                           left, name='butterfly_fix')

    # Define Solver
    solver = Solver(model)
    solver.form = model.formcollection.multi_body_crank_nicolson()

    # To keep u1 below 4 -> not allowing the disk to move out of frame
    solver.form += fe.dot(fe.conditional(fe.gt(
        fe.dot(disk.displacement_field.function, disk.displacement_field.function), 4
        ), disk.displacement_field.function, fe.Constant((0., 0.))
                                         ),
                          disk.displacement_field.test_function) * fe.dx
    solver.initialize()

    solver_options = {"nonlinear_solver": "snes",
                      "snes_solver" : {
                          "linear_solver": 'mumps',
                          "preconditioner": 'amg',
                          "maximum_iterations": 40,
                          "relative_tolerance": 1e-9,
                          "absolute_tolerance": 1e-9,
                          "error_on_nonconvergence": False},
                      }
    solver.update_options(solver_options)

    # ===================================================================

    with sim:

        # Grow the disk and solve for equilibrium
        for i, r in enumerate(range(params['nb_steps'])):
            
            backup = disk.phi_prev.vector()[:].copy()
            converged = False
            step_size_reducer = 1
            nb_tries = 0

            # If not converged, adaptively reduce step size
            while True:

                disk.phasefield.grow(params['grow_step']*step_size_reducer)
                disk.displacement_field.assign(fe.Constant((0., 0.)))

                solver.initialize()
                solver.update_options(solver_options)
                iterations, converged = solver.solve()

                if converged:
                    break
                else: 
                    step_size_reducer *= .5
                    nb_tries += 1
                    if MPI.COMM_WORLD.rank==0:
                        print('Not converged, reduce stepsize')
                    disk.phi_prev.vector()[:] = backup
                    disk.phasefield.assign(disk.phi_prev)
                    butterfly.displacement_field.assign(fe.Constant((0., 0.)))

                if nb_tries>2:
                    if MPI.COMM_WORLD.rank==0:
                        print('Not gonna work...')
                        raise RuntimeError('Simulation failed')
                    return

            # Store the step data
            sim.add_field('disk_phi', model.get_nodal_values(disk.phi))
            sim.add_field('disk_vonMises', model.get_nodal_values(disk.vonMises_stress()))
            sim.add_field('disk_sig', model.get_nodal_values(disk.stress()))
            sim.add_field('disk_b', model.get_nodal_values(eqs.contact_force_field(disk, butterfly)))
            sim.add_field('disk_X', model.get_nodal_values(disk.referencemap.function))
            sim.add_field('butter_phi', model.get_nodal_values(butterfly.phi))
            sim.add_field('butter_vonMises', model.get_nodal_values(butterfly.vonMises_stress()))
            sim.add_field('butter_sig', model.get_nodal_values(butterfly.stress()))
            sim.add_field('butter_b', model.get_nodal_values(eqs.contact_force_field(butterfly, disk)))
            sim.add_field('butter_X', model.get_nodal_values(butterfly.referencemap.function))

            sim.add_global_field('r', r)
            sim.add_global_field('energy', fe.assemble((butterfly.stress()[0, 0]**2 + butterfly.stress()[1, 1]**2
                                                        + butterfly.stress()[0, 1]**2) * fe.dx))
            sim.add_global_field('traction_x', fe.assemble(solver.penalty_body_force()[0] * fe.dx))
            sim.add_global_field('traction_y', fe.assemble(solver.penalty_body_force()[1] * fe.dx))

            # Update phase field & reference map
            disk.update_fields()
            butterfly.update_fields()

            sim.finish_step()
            sim.create_xdmf_file()

            if MPI.COMM_WORLD.rank==0:
                print(f'Solved step {i}')


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str)
    parser.add_argument('--uid', type=str)
    args = parser.parse_args()

    main(args.path, args.uid)



