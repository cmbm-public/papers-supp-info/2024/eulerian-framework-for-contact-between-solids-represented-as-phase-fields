# This file is part of the supplementary material for the publication:
# Eulerian framework for contact between solids represented as phase fields
# 
# Script for contact between a rigid plane and a deformable half-circle
# Uses:
#   - bamboost
#
# Copyright Flavio Lorez 2023

import os
import numpy as np

from bamboost import Manager

# Specify path of database
database_path = '../out'
db = Manager(database_path)

# E.g. create a list with mobilities to check
mob_list = [1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2]

for mob in mob_list:

    name = None  # random ID will be assigned
    script = os.path.abspath('./script_rigid.py')

    # Define parameter dictionary
    parameters = {
            'problem_type': 'cranck nicolson implicit',
            'tag': 'M study',
            'eps': 0.2,
            'mobility': mob,
            'r0': 10.,
            'E': 1.,
            'poissons': 0.2,
            'k': 1e3,
            'mesh': os.path.abspath('../mesh/mesh_hertz_2.xdmf'),
            'residual_stiffness': 1e-3,
            }

    rigid_motion = np.linspace(parameters['r0'], parameters['r0']-0.1, 6)
    parameters.update({'rigid_positions': rigid_motion})

    # Create new entry in database
    sim = db.create_simulation(name)
    sim.add_parameters(parameters)
    sim.copy_executable(script)
    sim.copy_file('./job_rigid.py')
    sim.create_batch_script(nnodes=1, ntasks=4,
                            ncpus=1, time='03:00:00', mem_per_cpu=2048, euler=False)

    print(sim.uid)

