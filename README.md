# Supplementary material

### Title
Eulerian framework for contact between solids represented as phase fields

### Authors
Flavio Lorez, Mohit Pundir, David S. Kammer

### Date
2023/09/29

### Description
The repository contains the scripts and jupyter notebooks to reproduce the results and the figures as used in the paper.
The data used in the paper (and in the notebooks here) can be downloaded from the ETH Research collection ([10.3929/ethz-b-000634803](10.3929/ethz-b-000634803)).

The data has been stored and managed using `bamboost`. A python package developed at CMBM using HDF5.
As library is still evolving, to make sure the notebooks and scripts work, install version `0.3.0`. It can be installed from the packaging index PyPI:
```bash
pip install bamboost=0.3.0
```

The package `phasontact` in this repository provides the modeling framework built on FEniCS.

#### Scripts
- File **scripts/job_*** are the executables which are used to setup the input files for the respective simulations.
- File **scripts/script_*** are the executables which produce the respective results.
    
#### Notebooks
- File **notebooks/figure_3.ipynb** reproduces the validation against the analytical hertz traction profile (Figure 3, Section 5.1).
- File **notebooks/figure_4.ipynb**  reproduces the figure addressing contact between a deformable plane and a deformable cylinder (Figure 4, Section 5.2).
- File **notebooks/figure_5.py**  reproduces the results for the large deformation example where a deformable disk compresses a deformable box (Figure 5, Section 5.3).
- File **notebooks/figure_6_7.ipynb**  reproduces the figures showing the butterfly example (Figure 6 & 7).
