# This file is part of phasontact, a Python library of the phase-field
# model for contact in an Eulerian framework
#
# https://gitlab.ethz.ch/compmechmat/research/corrosion/phasontact
#
# Copyright 2023 Flavio Lorez and contributors
#
# There is no warranty for this code

from __future__ import annotations
from typing import Iterable

import numpy as np
import fenics as fe

from phasontact.common import FUNCTION_SPACES


class Mesh:
    """Mesh object containing the Fenics mesh. Potentially expanded to adaptive mesh.

    Args:
        - ...

    Attributes:
        - ...
    """

    def __init__(self) -> None:
        self.msh = fe.Mesh()

    def read(self, file_name: str) -> Mesh:
        """Read a mesh from file.

        Args:
            - file_name: path to xdmf file containing fenics mesh
        """
        file = fe.XDMFFile(fe.MPI.comm_world, file_name)
        file.read(self.msh)
        return self

    def refine(self, *condition) -> Mesh:
        """
        Refine the mesh where the function is above threshold.

        Args:
            - *condition: lambda function taking coordinates x as argument.
        """
        cell_markers = fe.MeshFunction('bool', self.msh, self.msh.topology().dim())
        cell_markers.set_all(False)
        for c in fe.cells(self.msh):
            p = c.midpoint()
            p_xy = [p.x(), p.y()]
            if np.all([cond(p_xy) for cond in condition]):
                cell_markers[c] = True
            # if func(p_xy) > threshold: cell_markers[c] = True
        self.msh = fe.refine(self.msh, cell_markers, redistribute=True)
        return self

    def V(self, function_type: str = 'scalar', element_family: str = 'CG',
          element_degree: int = 1, **kwargs) -> fe.FunctionSpace:
        """Returns a new function space for this mesh.

        Args:
            - function_type: `scalar`, `vector` or `tensor` (default='scalar')
            - element_family: Element type, see Fenics docs (default='CG')
            - element_degree: Element degree, see Fenics docs (default=1)
        """
        space = FUNCTION_SPACES.get(function_type)
        return space(self.msh, element_family, element_degree, **kwargs)


class RectangularMesh(Mesh):
    """Create a simple rectangular mesh.

    Args:
        - domain: ((xmin, xmax), (ymin, ymax))
        - nb_nodes: number of nodes (nb_x, nb_y)
    """
    
    def __init__(self, domain: Iterable, nb_nodes: Iterable, **kwargs) -> None:
        self.msh = fe.RectangleMesh(fe.Point(domain[0][0], domain[1][0]),
                                    fe.Point(domain[0][1], domain[1][1]),
                                    nb_nodes[0], nb_nodes[1], **kwargs)


