# This file is part of phasontact, a Python library of the phase-field
# model for contact in an Eulerian framework
#
# https://gitlab.ethz.ch/compmechmat/research/corrosion/phasontact
#
# Copyright 2023 Flavio Lorez and contributors
#
# There is no warranty for this code

from abc import ABC, abstractmethod
import fenics as fe

from phasontact.common import Expr

class Material(ABC):
    """Abstract class for materials"""

    def __init__(self):
        pass

    @abstractmethod
    def strain(self, F: fe.Function):
        pass

    @abstractmethod
    def stress(self, F):
        pass

    @abstractmethod
    def strain_energy_density(self, F):
        pass


class LinearElastic(Material):
    """Linear elastic material in 2d.

    Args:
        - youngs_modulus (float): Youngs Modulus
        - poissons_ratio (float): Poisson's ratio
    """

    def __init__(self, youngs_modulus: float, poissons_ratio: float):
        self.youngs_modulus = youngs_modulus
        self.poisson = poissons_ratio
        self.lame_lambda = youngs_modulus*poissons_ratio / (1+poissons_ratio) / (1-2*poissons_ratio)
        self.lame_mu = youngs_modulus / 2 / (1+poissons_ratio)

    def strain(self, F: fe.Function) -> fe.Function:
        """Returns the Green strain tensor."""
        green_strain = fe.Constant(0.5) * (fe.dot(F.T, F) - fe.Identity(2))
        return green_strain

    def strain_energy_density(self, F: Expr):
        """Not implemented"""
        return 1/2*fe.inner(self.strain(F), self.stress(F))

    def stress(self, F: Expr) -> Expr:
        """Returns the second piola kirchhoff stress."""
        green_strain = self.strain(F)
        second_piola_kirchhoff = 2.0 * self.lame_mu * green_strain \
                + self.lame_lambda*fe.tr(green_strain)*fe.Identity(2)
        return second_piola_kirchhoff


class NeoHookeanElastic(Material):
    """Neo-Hookean material. Hyperelastic material for large strain.

    Args:
        - youngs_modulus: Young's modulus E
        - poissons_ratio: Poisson's ratio v

    Attributes:
        - ...
    """

    def __init__(self, youngs_modulus: float, poissons_ratio: float):
        self.youngs_modulus = youngs_modulus
        self.poisson = poissons_ratio
        self.lame_lambda = fe.Constant(youngs_modulus*poissons_ratio / 
                                       (1+poissons_ratio) / (1-2*poissons_ratio))
        self.lame_mu = fe.Constant(youngs_modulus / 2 / (1+poissons_ratio))

    def strain(self, F: fe.Function):
        """Returns the right cauchy green tensor"""
        return F.T * F
    
    def strain_energy_density(self, F):
        C = self.strain(F)
        J = fe.det(F)
        Ic = fe.tr(C)
        psi = self.lame_mu/2 * (Ic-2) - self.lame_mu*fe.ln(J) + self.lame_lambda/2*(fe.ln(J))**2
        return psi

    def stress(self, F: fe.Function):
        """Returns the 2nd piola kirchhoff stress."""
        C = self.strain(F)
        J = fe.det(F)
        second_piola_1 = self.lame_lambda * fe.ln(J) * fe.inv(C) 
        second_piola_2 = self.lame_mu * (fe.Identity(2) - fe.inv(C))
        return second_piola_1 + second_piola_2


class Rigid(Material):
    """Rigid material."""

    def __init__(self):
        pass
    def strain(self, F):
        pass
    def stress(self, F):
        pass
    def strain_energy_density(self, F):
        pass
