# This file is part of phasontact, a Python library of the phase-field
# model for contact in an Eulerian framework
#
# https://gitlab.ethz.ch/compmechmat/research/corrosion/phasontact
#
# Copyright 2023 Flavio Lorez and contributors
#
# There is no warranty for this code

import fenics as fe

try:
    from ufl_legacy.core.expr import Expr
    from ufl_legacy.indexed import Indexed
    from ufl_legacy.tensors import ListTensor
    from ufl_legacy.form import Form as uflForm
except ImportError:
    from ufl.core.expr import Expr
    from ufl.indexed import Indexed
    from ufl.tensors import ListTensor
    from ufl.form import Form as uflForm


FUNCTION_TYPES = {
        'scalar': fe.FiniteElement,
        'vector': fe.VectorElement,
        'tensor': fe.TensorElement,
        }
FUNCTION_SPACES = {
        'scalar': fe.FunctionSpace,
        'vector': fe.VectorFunctionSpace,
        'tensor': fe.TensorFunctionSpace,
        }
        
OPTS = {
        'penalty_constant': 1000,
        'residual_stiffness': 1e-3,
        }


def type_of_expression(expr: Expr) -> str:
    shape = expr.ufl_shape
    dimensionality = len(shape)
    if dimensionality==0:  # scalar
        return 'scalar'
    elif dimensionality==1:  # vector
        return 'vector'
    elif dimensionality==2:  # tensor, voigt notation
        return 'tensor'
