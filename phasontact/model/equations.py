# This file is part of phasontact, a Python library of the phase-field
# model for contact in an Eulerian framework
#
# https://gitlab.ethz.ch/compmechmat/research/corrosion/phasontact
#
# Copyright 2023 Flavio Lorez and contributors
#
# There is no warranty for this code

from __future__ import annotations
from typing import Tuple

import fenics as fe

from phasontact.model.body import Body
from phasontact.common import OPTS, Expr


def contact_force_field(body: Body, other: Body) -> Expr:
    """Get the contact force field."""
    n = other.phasefield.normal()
    phi = body.phasefield.function
    phi_other = other.phasefield.function
    force = fe.conditional(fe.gt(phi*phi_other, .25),
                           OPTS['penalty_constant'] * (phi*phi_other-.25) * -1*n,
                           fe.Constant((0., 0.)))
    return force


def contact_force(body: Body, other: Body, test_function: Expr = None) -> fe.Form:
    """Penalty contact body force.

    Args:
        - body: body to apply
        - other: body in contact
        - test_function: Optional, test space (vector, default=u)
    """
    force = contact_force_field(body, other)
    if test_function is None:
        test_function = body.displacement_field.test_function
    form = fe.dot(force, test_function) * fe.dx
    return form


def chemical_energy_phi(body: Body) -> fe.Form:
    """Second part of Cahn-Hilliard split equation.

    Args:
        - body: body to apply
    """
    phi, phi0, _ = body.phasefield.get_components()
    mu, _, mu_test = body.chemical_energy_mu.get_components()
    eps = body.eps

    form = (
            mu * mu_test * fe.dx
            # - eps**2 * fe.dot(fe.grad((phi+phi0)/2), fe.grad(mu_test)) * fe.dx
            - eps**2 * fe.dot(fe.grad(phi), fe.grad(mu_test)) * fe.dx
            - body.phasefield.g_ * mu_test * fe.dx
            )
    return form


def chemical_energy_phi_CN(body: Body) -> fe.Form:
    """Second part of Cahn-Hilliard split equation. Crank-Nicolson implicit.

    Args:
        - body: body to apply
    """
    phi, phi0, _ = body.phasefield.get_components()
    mu, _, mu_test = body.chemical_energy_mu.get_components()
    eps = body.eps

    form = (
            mu * mu_test * fe.dx
            - eps**2 * fe.dot(fe.grad((phi+phi0)/2), fe.grad(mu_test)) * fe.dx
            - body.phasefield.G_((phi+phi0)/2) * mu_test * fe.dx
            )
    return form


def chemical_energy_phi_with_elastic(body: Body) -> fe.Form:
    """Chemical energy with elastic strain energy."""
    phi, phi0, _ = body.phasefield.get_components()
    mu, _, mu_test = body.chemical_energy_mu.get_components()
    eps = body.eps

    form = (
            mu * mu_test * fe.dx
            - eps**2 * fe.dot(fe.grad((phi+phi0)/2), fe.grad(mu_test)) * fe.dx
            - body.phasefield.g_ * mu_test * fe.dx
            - .5 * fe.inner(body.stress(), body.strain()) * mu_test * fe.dx
            )
    return form


def advective_cahn_hilliard(body: Body, include_chemical: bool = True) -> fe.Form:
    """Advective Cahn-Hilliard equation.
    
    Args:
        - body: body to apply
        - include_chemical: if false, equation for mu is not written (if you want to
            specify it yourself...)
    """
    # Function aliases
    u = body.displacement_field.function
    phi, phi0, phi_test = body.phasefield.get_components()
    mu = body.chemical_energy_mu.function
    mobility = body.mobility

    form_phi = (
            fe.dot(u, fe.grad((phi+phi0)/2)) * phi_test * fe.dx
            + (phi-phi0) * phi_test * fe.dx
            + mobility * fe.dot(fe.grad(mu), fe.grad(phi_test)) * fe.dx
            )

    if include_chemical:
        return form_phi + chemical_energy_phi(body)
    return form_phi


def conservation_of_mass(body: Body, include_chemical: bool = True) -> fe.Form:
    """Advective Cahn-Hilliard equation with density as conserved quantity.

    Args:
        - body: body to apply
        - include_chemical: if true, equation for mu is not written (if you want to
            specify it yourself...)
    """
    u = body.displacement_field.function
    phi_test = body.phasefield.test_function
    mu = body.chemical_energy_mu.function
    mob = body.mobility
    rho, rho0 = body.density, body.density_prev

    form = (
            (rho-rho0) * phi_test * fe.dx
            + fe.dot(u, fe.grad((rho+rho0)/2)) * phi_test * fe.dx
            + (rho+rho0)/2 * fe.div(u) * phi_test * fe.dx
            + mob * fe.dot(fe.grad(mu), fe.grad(phi_test)) * fe.dx
            )
    if include_chemical:
        return form + chemical_energy_phi(body)
    return form


def advective_allen_cahn(body: Body) -> fe.Form:
    """Advective allen cahn equation.

    Args:
        - body: body to apply
    """
    u = body.displacement_field.function
    phi, phi0, phi_test = body.phasefield.get_components()
    eps = body.eps
    mobility = body.mobility

    form = (
            (phi-phi0) * phi_test * fe.dx
            + fe.dot(u, fe.grad((phi+phi0)/2)) * phi_test * fe.dx
            + mobility * eps**2 * fe.dot(fe.grad((phi+phi0)/2), fe.grad(phi_test)) * fe.dx
            + mobility * body.phasefield.g_ * phi_test * fe.dx
            - mobility * fe.sqrt(body.phasefield.g) * phi_test * fe.dx
            )
    return form


def reference_map_advection(body: Body) -> fe.Form:
    """Linear advection of reference map.

    Args:
        - body: body to apply
    """
    zeta, zeta0, zeta_test = body.referencemap.get_components()
    u = body.displacement_field.function

    form = (
            fe.dot((zeta-zeta0), zeta_test) * fe.dx
            + fe.dot(fe.dot(fe.grad((zeta+zeta0)/2), u), zeta_test) * fe.dx
            )
    return form


def static_equilibrium(body: Body, test_function: Expr = None) -> fe.Form:
    """Static equilibrium (div sigma = b).

    Args:
        - body: body to apply
        - test_function: Optional, test function
    """
    if test_function is None:
        test_function = body.displacement_field.test_function
    form = fe.inner(body.stress(), fe.grad(test_function)) * fe.dx
    return form


def static_equilibrium_contact(bodies: Tuple[Body], test_function: str = 'u') -> fe.Form:
    """Static equilibrium for all bodies including contact with other bodies.

    Args:
        - bodies: bodies to consider in contact
        - test_function: u or X
    """
    def get_test_func(body, test_function_str):
        if test_function_str=='u':
            return body.displacement_field.test_function
        elif test_function_str=='X':
            return body.referencemap.test_function
        else:
            return "What is that?"

    forms = list()
    for i, body in enumerate(bodies):
        test = get_test_func(body, test_function)
        form = static_equilibrium(body, test)
        for j, other in enumerate(bodies):
            if j==i:
                continue
            form += contact_force(body, other, test)
        forms.append(form)
    return sum(forms)

