# This file is part of phasontact, a Python library of the phase-field
# model for contact in an Eulerian framework
#
# https://gitlab.ethz.ch/compmechmat/research/corrosion/phasontact
#
# Copyright 2023 Flavio Lorez and contributors
#
# There is no warranty for this code

from __future__ import annotations

import fenics as fe

from phasontact.model.base import Model
from phasontact.model.form import Form


class Solver(object):
    """Object holding the solver. Meant for assembly of variational form and 
    implementation of solve loops.

    Args:
        - ...

    Attributes:
        - ...
    """
    form = Form()

    def __init__(self, model: Model) -> None:
        super().__init__()
        # self.form: Form = None
        self.model = model
        if hasattr(model, '_form'):
            self.form = model._form

    def initialize(self) -> Solver:
        """Initialize solver."""
        assert (self.form is not None), 'Define a form first'
        self.bcs = list(self.model.boundary_conditions.values())
        self.J = fe.derivative(self.form, self.model.solution, self.model.solution_trial)
        self.NonlinearVariationalProblem = fe.NonlinearVariationalProblem(
                self.form, self.model.solution, self.bcs, self.J)
        self.NonlinearVariationalSolver = fe.NonlinearVariationalSolver(
                self.NonlinearVariationalProblem)
        return self

    def update_options(self, options: dict) -> Solver:
        """Update solver parameters.

        Args:
            - options: dictionary with options, consult fenics/petsc docs
        """
        self.NonlinearVariationalSolver.parameters.update(options)
        return self

    def solve(self) -> tuple:
        """Solve the variational problem."""
        return self.NonlinearVariationalSolver.solve()
