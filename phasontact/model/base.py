# This file is part of phasontact, a Python library of the phase-field
# model for contact in an Eulerian framework
#
# https://gitlab.ethz.ch/compmechmat/research/corrosion/phasontact
#
# Copyright 2023 Flavio Lorez and contributors
#
# There is no warranty for this code

from __future__ import annotations
from itertools import combinations
from typing import Callable, Tuple, Iterable
import numpy as np
from scipy.interpolate import LinearNDInterpolator
from uuid import uuid4
from mpi4py import MPI

import fenics as fe

from phasontact.common import type_of_expression, Expr, Indexed, ListTensor
from phasontact.model.fieldvariable import FieldVariable
from phasontact.model.body import Body, RigidBody
from phasontact import material, geometry, mesh
from phasontact.model.form import FormCollection



class Model:
    """Parent object holding the model.

    Args:
        - params: Parameter dictionary. Will be to overwrite defaults.
        - mesh: yeah, totally the mesh (note that this is not the fenics object)

    Attributes:
        - bodies: dictionary containing all bodies of the model
        - boundary_conditions: dictionary containing all bcs of the model
    """

    def __init__(self, params: dict, mesh: mesh.Mesh) -> None:
        self.formcollection = FormCollection(self)
        self.params: dict = params
        self.mesh = mesh  # pointer to fenics mesh object
        self.bodies: dict = dict()
        self.bodies_rigid: dict = dict()
        self.boundary_conditions: dict = dict()

    def add_body(self, initial_shape: geometry.Geometry, material: material.Material,
                 eps: float = None, mobility: float = None, name: str = None) -> Body:
        """Add a body to the system.

        Args:
            - initial_shape: initial geometry of body
            - material: material of body
            - eps: interface width
            - mobility: mobility coefficient
            - name: Optional, name of body
        """
        if eps is None:
            eps = self.params.get('eps')
        if mobility is None:
            mobility = self.params.get('mobility')
        if name is None:
            name = uuid4().hex[:6]

        body = Body(initial_shape, material, eps=eps, mobility=mobility,
                    name=name, _mesh=self.mesh.msh)
        self.bodies[name] = body
        return body

    def add_rigid_body(self, initial_shape: geometry.Geometry, material: material.Material,
                       eps: float = None, mobility: float = None, name: str = None) -> Body:
        """Add a rigid body."""
        if eps is None:
            eps = self.params.get('eps')
        if mobility is None:
            mobility = self.params.get('mobility')
        if name is None:
            name = uuid4().hex[:6]

        body = RigidBody(initial_shape, material, eps=eps, mobility=mobility,
                         name=name, _mesh=self.mesh.msh)
        self.bodies_rigid[name] = body
        return body


    def generate_function_space(self) -> fe.FunctionSpace:
        """Generate the (mixed) function space for the defined system."""
        sub_spaces = list()  # gather all subspaces
        for body in self.bodies.values():
            sub_spaces.extend(body.solution_fields)

        self.mixed_element = fe.MixedElement([space.element for space in sub_spaces])
        self.function_space = fe.FunctionSpace(self.mesh.msh, self.mixed_element)
        self.solution = fe.Function(self.function_space)
        self.solution_trial = fe.TrialFunction(self.function_space)

        # distribute subspace to solution fields
        for i, (sub_space, split, test, trial) in enumerate(zip(
                sub_spaces, fe.split(self.solution), fe.TestFunctions(self.function_space),
                fe.TrialFunctions(self.function_space))):
            sub_space.link(split=split, test=test, trial=trial, mixed_function=self.solution,
                           _mixed_function_index=i)

        for body in self.bodies.values():
            body._post_function_space_generation()
        
        return self.solution

    def add_dirichlet_bc(self, field: FieldVariable, value: fe.Constant,
                         sub_domain: fe.CompiledSubDomain, name: str = None,
                         sub_field: str = None, *args) -> Model:
        """Add a dirichlet boundary condition.

        Args:
            - field: solution field to constrain
            - value: prescribed value
            - sub_domain: domain where to apply it
            - name: Optional, name for BC
            - sub_field: Optional, 'x' or 'y' will only apply it in this direction
            - *args: are passed to :function:`fe.DirichletBC()` (used for 'pointwise')
        """
        if name is None:
            name = uuid4().hex[:6]

        if isinstance(field.function, (Indexed, ListTensor)):
            space = field.get_function_space_as_sub()
        else:
            space = field.function.function_space()

        sub_fields = { 'x': 0, 'y': 1, 'z': 2 }
        if sub_field is not None:
            space = space.sub(sub_fields[sub_field])
        self.boundary_conditions[name] = fe.DirichletBC(space, value, sub_domain, *args)

    def get_nodal_values(self, func: Expr) -> np.ndarray:
        """Get the nodal values vector for any expression/function/field.
        Works in paralell by getting the vectors for the individual components for
        a vector or tensor expression/field.

        Args:
            - func: Expression/field/function
        """
        shape = func.ufl_shape
        dimensionality = len(shape)

        if dimensionality==0:  # scalar
            proj = fe.project(func)
            return proj.compute_vertex_values()
        elif dimensionality==1:  # vector
            components = [fe.project(func[i]).compute_vertex_values()
                          for i in range(shape[0])]
            return np.vstack(components).T
        elif dimensionality==2:  # tensor, voigt notation
            components = list()
            for i in range(shape[0]):
                components.append(fe.project(func[i, i]).compute_vertex_values())
            for index in combinations(range(shape[0]), 2):
                components.append(fe.project(func[index]).compute_vertex_values())
            return np.vstack(components).T

    def get_interpolator(self, expr: Expr, interpolator: Callable = LinearNDInterpolator) -> Callable:
        """Get a linear interpolator on the mesh domain for the given expression.
        Default is a linear interpolator.

        Args:
            expr: the expression/function to consider
            interpolator: callable where the first argument is points, second 
                argument is values.
        """
        try:
            values = expr.compute_vertex_values()
        except AttributeError:
            expr_type = type_of_expression(expr)
            expr = fe.project(expr, self.mesh.V(expr_type))
            values = expr.compute_vertex_values()
        coords = self.mesh.msh.coordinates()
        return interpolator(coords, values)

    def get_closest_vertex(self, x: Iterable, comm: MPI.Comm = MPI.COMM_WORLD) -> np.array:
        """Get the closest vertex to the position x.

        Args:
            - x: position of interest
            - comm: Optional, MPI communicator (default=MPI.COMM_WORLD)
        """
        dof_coords = self.function_space.tabulate_dof_coordinates()
        dof = np.argmin(np.linalg.norm(dof_coords - np.array(x), axis=1))
        closest_vertex_local = np.array(comm.allgather(dof_coords[dof]))
        if comm.rank==0:
            closest_vertex_global = closest_vertex_local[np.argmin(np.linalg.norm(
                closest_vertex_local-np.array(x), axis=1))]
        else:
            closest_vertex_global = None
        comm.barrier()
        closest_vertex_global = comm.bcast(closest_vertex_global, root=0)
        return closest_vertex_global
