# This file is part of phasontact, a Python library of the phase-field
# model for contact in an Eulerian framework
#
# https://gitlab.ethz.ch/compmechmat/research/corrosion/phasontact
#
# Copyright 2023 Flavio Lorez and contributors
#
# There is no warranty for this code

from uuid import uuid4
import fenics as fe

from phasontact.model.fieldvariable import FieldVariable, PhaseField, ReferenceMap, DisplacementField
from phasontact import geometry, material

from phasontact.common import OPTS


class RigidBody(object):
    """Rigid body"""

    def __init__(self, initial_shape: geometry.Geometry, material: material.Material,
                 eps: float, mobility: float, name: str = None, *, _mesh: fe.Mesh) -> None:
        if name is None:
            name = uuid4().hex[:6]
        self.name = name
        self.material = material
        self.eps = eps
        self.mobility = mobility
        self.phasefield = PhaseField(_mesh, initial_shape)
        self.phasefield.function = self.phasefield.function_prev
        self.initial_shape = initial_shape

    def redraw_phasefield(self) -> None:
        """Redraw the stationary phasefield."""
        fe.assign(self.phasefield.function,
                  fe.interpolate(self.initial_shape, self.phasefield.function_space())
                  )


class Body(object):
    """Stores a solid object. Contains the phase-field and the reference map.

    Args:
        - initial_shape: initial geometry of phase field
        - material: material of body
        - eps: interface width parameter
        - mobility: mobility of cahn-hilliard equation
        - name: Optional, name for body

    Attributes:
        - phasefield (:class:`~bamboost.model.fieldvariable.PhaseField`)
        - chemical_energy_mu (:class:`~bamboost.model.fieldvariable.FieldVariable`)
        - referencemap (:class:`~bamboost.model.fieldvariable.ReferenceMap`)
        - displacement_field (:class:`~bamboost.model.fieldvariable.DisplacementField`)
        - solution_fields (`list`)
        - residual_stiffness (float)
    """

    def __init__(self, initial_shape: geometry.Geometry, material: material.Material,
                 eps: float, mobility: float, name: str = None, *, _mesh: fe.Mesh) -> None:
        if name is None:
            name = uuid4().hex[:6]
        self.name = name
        self.material = material
        self.eps = eps
        self.mobility = mobility
        self.phasefield = PhaseField(_mesh, initial_shape)
        self.chemical_energy_mu = FieldVariable(_mesh, 'scalar', 'CG', 1)
        self.referencemap = ReferenceMap(_mesh)
        self.displacement_field = DisplacementField(_mesh)
        self.residual_stiffness = OPTS['residual_stiffness']
        self.initial_shape = initial_shape

        self.solution_fields = [
                self.displacement_field,
                self.phasefield,
                self.chemical_energy_mu,
                self.referencemap,
                ]

    def update_fields(self) -> None:
        """Update phasefield and reference map, ie. `function_prev = function`."""
        self.phasefield.assign_prev()
        self.referencemap.assign_prev()

    @property
    def phi(self) -> fe.Function:
        return self.phasefield.function

    @property
    def phi_prev(self) -> fe.Function:
        return self.phasefield.function_prev

    @property
    def density(self) -> fe.Function:
        return self.phasefield.function * fe.det(fe.grad(self.referencemap.function))

    @property
    def density_prev(self) -> fe.Function:
        return self.phasefield.function_prev * fe.det(fe.grad(self.referencemap.function_prev))

    def deformation_gradient(self) -> fe.Function:
        """Returns the deformation gradient computed from the reference map."""
        return fe.inv(fe.grad(self.referencemap.function))

    def strain(self, deformation_gradient: fe.Matrix = None) -> fe.Function:
        """Returns the strain of the body. No mixing here (only in stress)"""
        if deformation_gradient is None:
            deformation_gradient = self.deformation_gradient()
        return self.material.strain(deformation_gradient)

    def stress(self, deformation_gradient: fe.Matrix = None) -> fe.Function:
        """Mixing rule for stress response."""
        if deformation_gradient is None:
            deformation_gradient = self.deformation_gradient()
        return (fe.conditional(fe.gt(self.phasefield.function, self.residual_stiffness),
                               self.phasefield.function, self.residual_stiffness)
                * self.material.stress(deformation_gradient) )
        
    def vonMises_stress(self):
        """Calculate the von Mises stress intensity."""
        s = self.stress() - (1./3)*fe.tr(self.stress())*fe.Identity(2)  # deviatoric stress
        von_Mises = fe.sqrt(3./2*fe.inner(s, s))
        return von_Mises

    def _post_function_space_generation(self) -> None:
        """Bit of shortcutting. Assigns the displacement field as a function of
        zeta if it is not in the solution space.
        """
        if ((self.referencemap in self.solution_fields) and
            (self.displacement_field not in self.solution_fields)):
            self.displacement_field.function = (
                    -2 * fe.dot(fe.inv(fe.grad(self.referencemap.function + self.referencemap.function_prev)),
                               (self.referencemap.function - self.referencemap.function_prev))
                    )
        # u = fe.dot(fe.inv(fe.grad((zeta+zeta0)/2)), (zeta-zeta0))
