# This file is part of phasontact, a Python library of the phase-field
# model for contact in an Eulerian framework
#
# https://gitlab.ethz.ch/compmechmat/research/corrosion/phasontact
#
# Copyright 2023 Flavio Lorez and contributors
#
# There is no warranty for this code

from __future__ import annotations

from typing import Tuple

import fenics as fe

import phasontact.model.equations as eqs
from phasontact.model.body import Body
from phasontact.common import OPTS, uflForm


class Form:
    """Descriptor class for variational form. Assert form for assignment"""

    def __set_name__(self, owner, name):
        self.name = name

    def __get__(self, obj, type=None) -> fe.Form:
        return obj.__dict__.get(self.name)

    def __set__(self, obj, value) -> None:
        if not isinstance(value, uflForm):
            raise ValueError('This does not look like a UFL form.')
        obj.__dict__[self.name] = value


class FormCollection:
    """Collection of forms which we have used."""

    def __init__(self, parent=None):
        self.parent = parent

    def classic_multi_body(self, bodies: Tuple[Body] = None) -> fe.Form:
        """The full form used in Paper 1.

        Args:
            - bodies: Iterable with all bodies of the system.
        """
        bodies = self._get_bodies(bodies)
        forms = list()
        for i, body in enumerate(bodies):
            form = (eqs.static_equilibrium(body) + eqs.advective_cahn_hilliard(body)
                    + eqs.reference_map_advection(body))
            for j, other in enumerate(bodies):
                if j==i:
                    continue
                form -= eqs.contact_force(body, other)
            for rigid_other in self._get_bodies_rigid():
                form -= eqs.contact_force(body, rigid_other)
            forms.append(form)
        return sum(forms)

    def multi_body_crank_nicolson(self, bodies: Tuple[Body] = None) -> fe.Form:
        """The full form in Paper 1 (revised). Using crank-Nicolson also for mu

        Args:
            bodies: Iterable with all bodies to consider
        """
        bodies = self._get_bodies(bodies)
        forms = list()
        for i, body in enumerate(bodies):
            form = (eqs.static_equilibrium(body) + eqs.advective_cahn_hilliard(body, include_chemical=False)
                    + eqs.reference_map_advection(body) + eqs.chemical_energy_phi_CN(body))
            for j, other in enumerate(bodies):
                if j==i:
                    continue
                form -= eqs.contact_force(body, other)
            for rigid_other in self._get_bodies_rigid():
                form -= eqs.contact_force(body, rigid_other)
            forms.append(form)
        return sum(forms)

    def classic_multi_body_no_disp_field(self, bodies: Tuple[Body] = None) -> fe.Form:
        """testing idea to completely remove u."""
        bodies = self._get_bodies(bodies)
        forms = list()
        for i, body in enumerate(bodies):
            form = (eqs.static_equilibrium(body, test_function=body.referencemap.test_function)
                    + eqs.advective_cahn_hilliard(body)
                    )
            for j, other in enumerate(bodies):
                if j==i:
                    continue
                form += eqs.contact_force(body, other,
                                          test_function=body.referencemap.test_function)
            forms.append(form)
        return sum(forms)
            
    def _get_bodies(self, bodies = None) -> Tuple[Body]:
        if bodies is None:
            bodies = self.parent.bodies.values()
        return bodies

    def _get_bodies_rigid(self, bodies = None) -> Tuple[Body]:
        if bodies is None:
            bodies = self.parent.bodies_rigid.values()
        return bodies

