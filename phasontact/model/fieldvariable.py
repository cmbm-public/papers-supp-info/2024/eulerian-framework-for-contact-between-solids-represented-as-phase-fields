# This file is part of phasontact, a Python library of the phase-field
# model for contact in an Eulerian framework
#
# https://gitlab.ethz.ch/compmechmat/research/corrosion/phasontact
#
# Copyright 2023 Flavio Lorez and contributors
#
# There is no warranty for this code

from __future__ import annotations
from ctypes import ArgumentError
from itertools import combinations
from typing import Union

import fenics as fe
import numpy as np
from uuid import uuid4

from phasontact.common import FUNCTION_TYPES, FUNCTION_SPACES
from phasontact import geometry


# class FunctionDescriptor:
#     """Descriptor class for field variable solution. If the field is not a
#     solution subfield, try to access the `_function` (alternative description, 
#     e.g. u(zeta) for the displacement field)
#     """
#
#     def __set_name__(self, owner, name):
#         self.name = name
#
#     def __get__(self, obj, type=None) -> fe.Function:
#         if obj.is_solution:
#             return obj.__dict__.get(self.name)
#         else:
#             return obj.__dict__.get('_' + self.name)
#
#     def __set__(self, obj, value) -> None:
#         obj.__dict__['_' + self.name] = value


class FieldVariable(object):
    """Stores a Fenics solution field, which may be a subfield of a mixed Function.

    Args:
        - mesh: The mesh of the space
        - function_type: `scalar`, `vector` or `tensor`
        - element_family: Element type, see Fenics docs (default=`CG`)
        - element_degree: Element degree, see Fenics docs (default=1)
        - name: Optional, custom name for the field

    Attributes:
        - function: The solution function, a split function of the bigger mixed
            function.
        - function_prev: The function at the previous step (`t = i-1`)
    """
    is_solution = False
    # function = FunctionDescriptor()

    def __init__(self, mesh: fe.Mesh, function_type: str, element_family: str = 'CG',
                 element_degree: int = 1, name: str = None) -> None:
        self.mesh: fe.Mesh = mesh
        self.function_type = function_type
        self.element_degree = element_degree
        self.element_family = element_family
        if name is None:
            name = uuid4().hex[:6]
        self.name = name
        self.element = FUNCTION_TYPES.get(function_type)(
                element_family, mesh.ufl_cell(), element_degree
                )

        # function = solution, function_prev = old from previous step
        # function will be linked afterwards to be a subspace of a mixed function
        # space, using the link method
        self.function = None
        self.function_prev = fe.Function(self.function_space())
    
    def function_space(self) -> fe.FunctionSpace:
        """Return the function space of this field."""
        return fe.FunctionSpace(self.mesh, self.element)

    def link(self, split: fe.Function, test: fe.Function, trial: fe.Function,
             mixed_function: fe.Function, _mixed_function_index: int) -> None:
        """Link the subspace information of the solution space.

        Args:
            - split: The function got by `fe.split()`
            - test: The corresponding test function
            - trial: The corresponding trial function
        """
        self.is_solution = True
        self.function = split
        self.test_function = test
        self.trial_function = trial
        self.mixed_function = mixed_function
        self._mixed_function_index = _mixed_function_index
        # initialize function to previous initial values
        self._post_link_initialize()

    def _post_link_initialize(self) -> None:
        pass

    def get_as_sub(self) -> fe.Function:
        """Returns `function` as the indexed version of mixed space."""
        return self.mixed_function.sub(self._mixed_function_index)

    def get_function_space_as_sub(self) -> fe.FunctionSpace:
        """Returns the function space as the indexed version of mixed space."""
        return self.mixed_function.function_space().sub(self._mixed_function_index)

    def assign(self, new: Union(fe.Function, fe.Constant)) -> None:
        """Assign a new value for the field. Can be constant or function.

        Args:
            - new: New value to assign
        """
        try:
            fe.assign(self.get_as_sub(), new)
        except (AttributeError, RuntimeError):
            new = fe.project(new, self.function_space())
            fe.assign(self.get_as_sub(), new)

    def assign_prev(self, new: Union(fe.Function, fe.Constant) = None) -> None:
        """Assign a new value to the previous field.

        Args:
            - new: New value to assign (Default = `self.function`)
        """
        if new is None:
            new = self.get_as_sub()
        try:
            fe.assign(self.function_prev, new)
        except AttributeError:
            new = fe.project(new, self.function_space())
            fe.assign(self.function_prev, new)

    def get_nodal_values(self) -> np.ndarray:
        """Get the nodal values vector. Works in paralell by getting the vectors
        for the individual components for a vector or tensor expression/field.
        """
        shape = self.function.ufl_shape
        dimensionality = len(shape)

        if dimensionality==0:  # scalar
            proj = fe.project(self.function)
            return proj.compute_vertex_values()
        elif dimensionality==1:  # vector
            components = [fe.project(self.function[i]).compute_vertex_values()
                          for i in range(shape[0])]
            return np.vstack(components).T
        elif dimensionality==2:  # tensor, voigt notation
            components = list()
            for i in range(shape[0]):
                components.append(fe.project(self.function[i, i]).compute_vertex_values())
            for index in combinations(range(shape[0]), 2):
                components.append(fe.project(self.function[index]).compute_vertex_values())
            return np.vstack(components).T

    def get_components(self) -> tuple:
        """Returns function, function_prev and test_function."""
        return self.function, self.function_prev, self.test_function


class DisplacementField(FieldVariable):
    """Displacement increment field describing the motion.

    Args:
        - ...

    Attributes:
        - ...
    """

    def __init__(self, mesh: fe.Mesh) -> None:
        super().__init__(mesh, function_type='vector', element_family='CG',
                         element_degree=1)


class PhaseField(FieldVariable):
    """Phase field of a body defining its domain and interface.

    Args:
        - mesh: Fenics mesh
        - initial_shape: The geometry

    Attributes:
        - ...
    """

    def __init__(self, mesh: fe.Mesh, initial_shape: geometry.Geometry) -> None:
        super().__init__(mesh, function_type='scalar', element_family='CG',
                         element_degree=1)
        self.initial_shape = initial_shape
        self.initialize()

    def initialize(self, initial_shape: geometry.Geometry = None) -> PhaseField:
        """Initialize the field to some geometry.

        Args:
            - initial_shape: The geometry
        """
        if initial_shape is None:
            initial_shape = self.initial_shape
        self.assign_prev(fe.interpolate(initial_shape, self.function_space()))

    def _post_link_initialize(self) -> None:
        """Assigns function_prev to function."""
        self.assign(self.function_prev)

    @property
    def g(self) -> fe.Function:
        """Double well potential."""
        return self.function**2 * (1-self.function)**2

    @property
    def g_(self) -> fe.Function:
        """Double well potential prime (dg/dphi)."""
        return 2*self.function + 4*self.function**3 - 6*self.function**2

    def G(self, func) -> fe.Function:
        """Double well potential of some function"""
        return func**2 * (1-func)**2

    def G_(self, func) -> fe.Function:
        """Double well potential prime of some function."""
        return 2*func + 4*func**3 - 6*func**2

    def normal(self) -> fe.Function:
        """Normal of phasefield."""
        threshold = fe.Constant(1e-18)
        grad = fe.grad(self.function)
        length = fe.sqrt(fe.dot(grad, grad))
        return fe.conditional(fe.gt(length, threshold), grad/length, grad/threshold)

    def grow(self, grow_size: float = 1e-2) -> None:
        """Grow phi by linear advection in normal direction.

        Args:
            grow_size (float): parameter controlling the grow step size
        """
        grad = fe.grad(self.function_prev)
        length = fe.sqrt(fe.dot(grad, grad))
        normal = fe.conditional(fe.gt(length, 1e-10), grad / length, grad/fe.Constant(1e-10))
        phi_new = self.function_prev + fe.dot(grad, normal*grow_size)
        fe.assign(self.function_prev, fe.project(phi_new, self.model.V(1)))
        self.assign(self.function_prev)


class ReferenceMap(FieldVariable):
    """Reference map storing the reference configuration of a body.

    Args:
        - ...

    Attributes:
        - ...
    """

    def __init__(self, mesh: fe.Mesh) -> None:
        super().__init__(mesh, function_type='vector', element_family='CG',
                         element_degree=1)
        self.initialize()
        
    def initialize(self) -> ReferenceMap:
        """initialize the reference map `$\zeta = x$`"""
        self.assign_prev(fe.interpolate(geometry.InitRM(), self.function_space()))

    def _post_link_initialize(self) -> None:
        """Assigns function_prev to function."""
        self.assign(self.function_prev)

