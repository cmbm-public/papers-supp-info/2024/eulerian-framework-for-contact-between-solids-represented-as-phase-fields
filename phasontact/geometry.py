# This file is part of phasontact, a Python library of the phase-field
# model for contact in an Eulerian framework
#
# https://gitlab.ethz.ch/compmechmat/research/corrosion/phasontact
#
# Copyright 2023 Flavio Lorez and contributors
#
# There is no warranty for this code

from abc import abstractmethod
from typing import Iterable
import fenics as fe
import numpy as np


class Geometry(fe.UserExpression):
    """Base interface class. Initial configuration for the phase field variables."""

    def __init__(self):
        super().__init__()

    @abstractmethod
    def eval(self, values, x):
        """assigning values to position x, consult fenics docs."""
        pass

    @abstractmethod
    def value_shape(self):
        """Shape of UserExpression, consult fenics docs."""
        pass


class FromImage(Geometry):
    """Create an object from an image file. Uses the fast marching method to compute
    the signed distance function from the sharp interface location given by binary
    image.

    Args:
        - fname (str): path to image file.
        - domain (list(list)): domain of the image
        - eps (float): interface width parameter for phase-field

    Attributes:
        - ...
    """

    def __init__(self, fname: str, domain: list, eps: float):
        super().__init__()

        import imageio
        from scipy import interpolate
        import skfmm

        im = imageio.imread(fname)
        ima = np.array(im)
        ima = ima[::-1]
        res = ima.shape

        # Differentiate inside/outside region
        phi = np.int64(np.any(ima[:, :, :3], axis=2))
        phi = np.where(phi, 1, -1)

        # Compute signed distance to 0 level
        xi, yi = np.linspace(*domain[0], res[1]), np.linspace(*domain[1], res[0])
        pixel_length = (domain[0][1]-domain[0][0])/res[1]
        sd = skfmm.distance(phi, dx=pixel_length)

        self.sd = interpolate.RegularGridInterpolator((yi, xi), sd)
        self.eps = eps

    def eval(self, values, x):
        values[0] = .5*(1-np.tanh(self.sd((x[1], x[0]))/np.sqrt(2)/self.eps))

    def value_shape(self):
        return ()


class Circular(Geometry):
    """Circular shape with given radius and interface width.

    Args:
        - `r0` (float): the radius of the object
        - `eps` (float): the interface width parameter
        - `center` (tuple<float> = (0, 0)): the center of the object
        - `inverted` (bool): if true, then `phi` will be zero inside and 1 outside

    Attributes:
        - ...
    """
    def __init__(self, r0: float, eps: float, center: Iterable = (0, 0), inverted: bool = False):
        super().__init__()
        self.r0, self.eps = r0, eps
        self.center = center
        self.INVERTED = inverted

    def eval(self, values, x):
        profile = .5*(1-np.tanh(
            (fe.sqrt((x[0] - self.center[0])**2 + (x[1] - self.center[1])**2) - (self.r0))/np.sqrt(2) / self.eps))
        if self.INVERTED:
            values[0] = (1 - profile)
        else:
            values[0] = profile

    def value_shape(self):
        return ()


class VerticalWallsSymmetric(Geometry):
    """Vertical walls with given radius and interface width.

    Args:
        - `r0` (float): the radius of the object
        - `eps` (float): the interface width parameter
        - axis (str): 'x', 'y' = orientation of plane

    Attributes:
        - ...
    """
    def __init__(self, r0: float, eps: float, axis='x'):
        super().__init__()
        self.r0, self.eps = r0, eps
        self.axis = axis

    def eval(self, values, x):
        signed_distance = np.abs({'x': x[0], 'y': x[1]}.get(self.axis)) - self.r0
        values[0] = .5 + .5*np.tanh(signed_distance/np.sqrt(2) / self.eps)

    def value_shape(self):
        return ()


class Plane(Geometry):
    """Plane defined by y coordinate smaller than specified.

    Args:
        - y: interface height
        - eps: the interface width parameter

    Attributes:
        - ...
    """
    def __init__(self, y: float, eps: float):
        super().__init__()
        self.y = y
        self.eps = eps

    def eval(self, values, x):
        values[0] = .5 - .5*np.tanh((x[1] - self.y)/np.sqrt(2) / self.eps)

    def value_shape(self):
        return ()


class Box(Geometry):
    """Box shape.

    Args:
        - x_range: x interval
        - y_range: y interval
        - eps: interface width parameter

    Attributes:
        - ...
    """
    def __init__(self, x_range: Iterable, y_range: Iterable, eps: float,
                 domain=[[-50, 50], [-50, 50]], n=1000):
        super().__init__()

        from scipy import interpolate
        import skfmm

        # Compute signed distance to 0 level
        xmin, xmax = x_range
        ymin, ymax = y_range

        xi, yi = np.linspace(*domain[0], n), np.linspace(*domain[1], n)
        xx, yy = np.meshgrid(xi, yi)
        box = np.where((xx>xmin) & (xx<xmax) & (yy>ymin) & (yy<ymax), -1, 1)
        
        sd = skfmm.distance(box, dx=(domain[0][1]-domain[0][0])/n)

        self.sd = interpolate.RegularGridInterpolator((xi, yi), sd)
        self.eps = eps

    def eval(self, values, x):
        values[0] = .5*(1-np.tanh(self.sd((x[1], x[0]))/np.sqrt(2)/self.eps))

    def value_shape(self):
        return ()


class InitRM(fe.UserExpression):
    """UserExpression to initialize the Reference Map."""

    def eval(self, values, x):
        values[0] = x[0]
        values[1] = x[1]
    def value_shape(self):
        return (2,)

